//
//  TipoDeCambioPresenter.swift
//  IOSChallengeBCP
//
//  Created by Jhonly Junior Garcia Pitoy on 16/12/21.
//

import UIKit

protocol TipoDeCambioPresentation {
    // GOTO
    func GotoListaDeMoneda(vc: UIViewController)
    // PREPARE
//    func prepareUserDataFromCD()
}

//MARK: - Protocol Tags
//protocol TipoDeCambioPresentationTags {
//    func trackStateShowScreen()
//    func trackActionMetadata(key: String, value: String)
//    func trackActionClickButton(name: String, label: String)
//    func trackEvent()
//    func trackEventClickButton(name: String, label: String)
//    func trackActionError(errorCode: String, errorMessage: String)
//}

class TipoDeCambioPresenter {
    weak var view: TipoDeCambioView?
    var router: TipoDeCambioRouting

    //var data: [TipoDeCambio] = []

    init(view: TipoDeCambioView, router: TipoDeCambioRouting) {
        self.view = view
        self.router = router
    }
}

extension TipoDeCambioPresenter: TipoDeCambioPresentation {

//     MARK: - GO TO
    func GotoListaDeMoneda(vc: UIViewController) {
        self.router.GotoListaDeMoneda(vc : vc)
    }

    // MARK: - PREPARE
//    func prepareUserDataFromCD() {
//            _ = firstly {
//                self.interactor.retrieveUserDataFromCD()
//            }.done { [weak self] result in
//                // let message = result.response?.message
//                // if let response = result.response, message?.codigo == CodeConstants.success {
//                if let personasBuscar = result.personasBuscar {
//                    self?.view?.successUserDataFromCD(persona: personasBuscar)
//                } else {
//                    self?.view?.failedUserDataFromCD()
//                    // NewGeneralPopUp().showError(delegate: self?.view, messageFailed: message)
//                }
//            }.catch({ [weak self] error in
//                self?.view?.failedUserDataFromCD()
//                NewGeneralPopUp().showError(delegate: self?.view, error: error)
//            })
//    }
}

// MARK: - TAGS
//extension TipoDeCambioPresenter: TipoDeCambioPresentationTags {
//    func trackStateShowScreen() {
//        TrackStateBuilder(
//            appScreenCategory: "TipoDeCambio",
//            appScreenSubcategory: "TipoDeCambio",
//            appSessionStatus: Constans.appSesionStatusLoggedIn,
//            trackState: "TipoDeCambio")
//            .withUserDigitalId()
//            .send()
//    }
//
//    func trackActionMetadata(key: String, value: String) {
//        TrackActionBuilder(
//            appSessionStatus: Constans.appSesionStatusLoggedIn,
//            trackAction: Constans.trackActionMetadata)
//            .withUserDigitalId()
//            .withMetadataKey(key.unaccent())
//            .withMetadataValue(value.unaccent())
//            .send()
//    }
//
//    func trackActionClickButton(name: String, label: String) {
//        TrackActionBuilder(
//            appSessionStatus: Constans.appSesionStatusLoggedIn,
//            trackAction: Constans.trackAction)
//            .withUserDigitalId()
//            .withActionGroup("TipoDeCambio")
//            .withActionCategory("TipoDeCambio")
//            .withActionName(name.unaccent())
//            .withActionLabel(label.unaccent())
//            .send()
//    }
//
//    func trackEvent() {
//        TrackActionBuilder(
//            appSessionStatus: Constans.appSesionStatusLoggedIn,
//            trackAction: Constans.trackAction)
//            .withUserDigitalId()
//            .withEventName(Constans.appEventNameTrackAction)
//            .send()
//    }
//
//    func trackEventClickButton(name: String, label: String) {
//        TrackActionBuilder(
//            appSessionStatus: Constans.appSesionStatusLoggedIn,
//            trackAction: Constans.trackAction)
//            .withUserDigitalId()
//            .withEventName(Constans.appEventNameTrackAction)
//            .withActionGroup("TipoDeCambio")
//            .withActionCategory("TipoDeCambio")
//            .withActionName(name.unaccent())
//            .withActionLabel(label.unaccent())
//            .send()
//    }
//
//    func trackActionError(errorCode: String, errorMessage: String) {
//        TrackActionBuilder.showError(code: errorCode.unaccent(), message: errorMessage.unaccent())
//    }
//}
