//
//  TipoDeCambioRouter.swift
//  IOSChallengeBCP
//
//  Created by Jhonly Junior Garcia Pitoy on 16/12/21.
//

import UIKit

protocol TipoDeCambioRouting {
    func GotoListaDeMoneda(vc: UIViewController)
}

class TipoDeCambioRouter {

    weak var view: TipoDeCambioViewController?

    init(view: TipoDeCambioViewController) {
        self.view = view
    }
}

extension TipoDeCambioRouter: TipoDeCambioRouting {
    func GotoListaDeMoneda(vc: UIViewController) {
        let vc = ListaDeMonedasAssembly.build(vc: vc)
        let navVC = UINavigationController(rootViewController: vc)
        navVC.isNavigationBarHidden = true
        navVC.modalPresentationStyle = .fullScreen
        self.view?.present(navVC, animated: true, completion: nil)
    }
}
