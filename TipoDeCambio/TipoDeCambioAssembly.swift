//
//  TipoDeCambioAssembly.swift
//  IOSChallengeBCP
//
//  Created by Jhonly Junior Garcia Pitoy on 16/12/21.
//
import UIKit

class TipoDeCambioAssembly {
	static func build(/*data: [TipoDeCambio]*/) -> TipoDeCambioViewController {

		let storyboard = UIStoryboard(name: "TipoDeCambio", bundle: Bundle.main)
		let view = storyboard.instantiateInitialViewController() as! TipoDeCambioViewController

//		let repository = Repository()
//		let interactor = TipoDeCambioInteractor(repository: repository)
		let router = TipoDeCambioRouter(view: view)
		let presenter = TipoDeCambioPresenter(view: view, router: router)

		//view.data = data
		view.presenter = presenter
		return view
	}
}
