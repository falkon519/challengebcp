//
//  TipoDeCambioViewController.swift
//  IOSChallengeBCP
//
//  Created by Jhonly Junior Garcia Pitoy on 16/12/21.
//

import UIKit
//import Hero
//import FloatingPanel
//import SkeletonView

protocol TipoDeCambioView: class {
    // SUCCESS-FAILED
    func successUserDataFromCD(persona: String)
    func failedUserDataFromCD()
    // METHODS
}

class TipoDeCambioViewController: UIViewController {

    var presenter: TipoDeCambioPresentation?

    @IBOutlet weak var firstCoinButton: UIButton!
    @IBOutlet weak var secondCoinButton: UIButton!
    

    //MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    // MARK: - INIT
    private func initView() {
        updateFont()
        seputView()
        isShimmer(true)
        getRepository()
    }

    private func updateFont() {
        
    }
    private func seputView() {
        let longGestureFirstButton = UILongPressGestureRecognizer(target: self, action: #selector(longFirstButton))
        let longGestureSecondButton = UILongPressGestureRecognizer(target: secondCoinButton, action: #selector(longSecondButton))
        
        firstCoinButton.addGestureRecognizer(longGestureFirstButton)
        secondCoinButton.addGestureRecognizer(longGestureSecondButton)
      
    }
    private func isShimmer(_ active: Bool) { }
    private func getRepository() { }

    
    
//    OUTLETS
    
    @IBAction func DolaresButtonTapped(_ sender: Any) {
        print("Action")
    }
    
    @IBAction func SolesButtonTapped(_ sender: Any) {
        print("Action")
    }
    
    @objc func longFirstButton() {
        self.presenter?.GotoListaDeMoneda(vc: self)
    }

    @objc func longSecondButton() {
        self.presenter?.GotoListaDeMoneda(vc: self)
    }
    

}

// MARK: - EXTENSION


extension TipoDeCambioViewController: TipoDeCambioView {
    // MARK: - SUCCESS-FAILED
    func successUserDataFromCD(persona: String) { }
    func failedUserDataFromCD() {}

    // MARK: - METHODS
}


extension TipoDeCambioViewController : ListaDeMonedasViewDelegate{
    func TapCountry(countrie: ListCountries) {
        print("Actualizar el cambio de moneda de acuerod al pais", countrie)
    }
    
    
}
