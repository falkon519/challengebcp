//
//  ListaDeMonedasViewController.swift
//  IOSChallengeBCP
//
//  Created by Jhonly Junior Garcia Pitoy on 16/12/21.
//

import UIKit
//import Hero
//import FloatingPanel
//import SkeletonView



protocol ListaDeMonedasViewDelegate: class {
    // SUCCESS-FAILED
    func TapCountry( countrie : ListCountries)
    // METHODS
}


protocol ListaDeMonedasView: class {
    // SUCCESS-FAILED
    func updateListOfCountries( countries : Countries)
    func retrieveListOfCountries()
    // METHODS
}

class ListaDeMonedasViewController: UIViewController {

    var presenter: ListaDeMonedasPresentation?
    var countries : Countries?
    var delegate : ListaDeMonedasViewDelegate!
    
    @IBOutlet weak var ListaDeMonedasTableView: UITableView!{
        willSet {
            newValue.dataSource = self
            newValue.delegate = self
            newValue.register(ListaDeMonedasCell.self)
        }
    }
    
    
    //MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
        self.retrieveListOfCountries()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        view.layoutSkeletonIfNeeded()
    }

    // MARK: - INIT
    private func initView() {
        updateFont()
        seputView()
        isShimmer(true)
        getRepository()
    }

    private func updateFont() { }
    private func seputView() {
    }
    private func isShimmer(_ active: Bool) { }
    private func getRepository() { }

    // MARK: - IBACTION
}



extension ListaDeMonedasViewController: ListaDeMonedasView {
    // MARK: - SUCCESS-FAILED
    func updateListOfCountries( countries : Countries){
        self.countries = countries
        self.ListaDeMonedasTableView.reloadData()
    }
    func retrieveListOfCountries() {
        self.presenter?.retrieListOfCounties()
    }

    // MARK: - METHODS
}




extension ListaDeMonedasViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _countires = self.countries{
            return _countires.ListCountries?.count ?? 0
        }else{
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell: ListaDeMonedasCell = tableView.dequeueCell(forIndexPath: indexPath)
        cell.displayCell(Title: countries?.ListCountries?[indexPath.row].name ?? ""  , Detalle: "ds")
        return cell
    }
}

extension ListaDeMonedasViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate.TapCountry(countrie: (self.countries?.ListCountries![indexPath.row])!)
    }
}
