//
//  ListaDeMonedasInteractor.swift
//  IOSChallengeBCP
//
//  Created by Jhonly Junior Garcia Pitoy on 16/12/21.
//

import UIKit

protocol ListaDeMonedasUseCase {
    func retrieListOfCounties() -> Countries
}
class ListaDeMonedasInteractor {
    //	var repository: RepositoryDelegate?
    
    init() {
    }
}

extension ListaDeMonedasInteractor: ListaDeMonedasUseCase {
    
    func retrieListOfCounties() -> Countries {
        let jsonData = readLocalJSONFile(forName: "countries")
        
        do{
            if let data = jsonData {
                if let sampleRecordObj = parse(jsonData: data) {
                    print("users list: \(sampleRecordObj)")
                    return sampleRecordObj
                }
            }
        }catch {
            print("error: \(error)")
        }
        return Countries(ListCountries: [ListCountries(valueofdolar: 0.0, imagen: "", moneda: "", name: "")])
        
    }
    
    
    
    func parse(jsonData: Data) -> Countries? {
        do {
            let decodedData = try JSONDecoder().decode(Countries.self, from: jsonData)
            return decodedData
        } catch {
            print("error: \(error)")
        }
        return nil
    }
    
    
    func readLocalJSONFile(forName name: String) -> Data? {
        do {
            if let filePath = Bundle.main.path(forResource: name, ofType: "json") {
                let fileUrl = URL(fileURLWithPath: filePath)
                let data = try Data(contentsOf: fileUrl)
                return data
            }
        } catch {
            print("error: \(error)")
        }
        return nil
    }
    
    
}
