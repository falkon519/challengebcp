//
//  ListaDeMonedasRouter.swift
//  IOSChallengeBCP
//
//  Created by Jhonly Junior Garcia Pitoy on 16/12/21.
//

import UIKit

protocol ListaDeMonedasRouting {
    func goToNextView()
}

class ListaDeMonedasRouter {

    weak var view: ListaDeMonedasViewController?

    init(view: ListaDeMonedasViewController) {
        self.view = view
    }
}

extension ListaDeMonedasRouter: ListaDeMonedasRouting {
    func goToNextView() {
        // let vc = ListaDeMonedasAssembly.build()
        // self.view?.navigationController?.pushViewController(vc, animated: true)
    }
}
