//
//  ListaDeMonedasAssembly.swift
//  IOSChallengeBCP
//
//  Created by Jhonly Junior Garcia Pitoy on 16/12/21.
//
import UIKit

class ListaDeMonedasAssembly {
	static func build(vc: UIViewController) -> ListaDeMonedasViewController {

		let storyboard = UIStoryboard(name: "ListaDeMonedas", bundle: Bundle.main)
		let view = storyboard.instantiateInitialViewController() as! ListaDeMonedasViewController

//		let repository = Repository()
		let interactor = ListaDeMonedasInteractor()
		let router = ListaDeMonedasRouter(view: view)
		let presenter = ListaDeMonedasPresenter(view: view, interactor: interactor, router: router)
		//view.data = data
		view.presenter = presenter
        view.delegate = vc as! ListaDeMonedasViewDelegate
		return view
	}
}
