//
//  ListaDeMonedasPresenter.swift
//  IOSChallengeBCP
//
//  Created by Jhonly Junior Garcia Pitoy on 16/12/21.
//

import UIKit

protocol ListaDeMonedasPresentation {
    // GOTO
    func goToNextView()
    // PREPARE
    func retrieListOfCounties()
}

//MARK: - Protocol Tags
protocol ListaDeMonedasPresentationTags {
//    func trackStateShowScreen()
//    func trackActionMetadata(key: String, value: String)
//    func trackActionClickButton(name: String, label: String)
//    func trackEvent()
//    func trackEventClickButton(name: String, label: String)
//    func trackActionError(errorCode: String, errorMessage: String)
}

class ListaDeMonedasPresenter {
    weak var view: ListaDeMonedasView?
    var interactor: ListaDeMonedasUseCase
    var router: ListaDeMonedasRouting

    //var data: [ListaDeMonedas] = []

    init(view: ListaDeMonedasView, interactor: ListaDeMonedasUseCase, router: ListaDeMonedasRouting) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
}

extension ListaDeMonedasPresenter: ListaDeMonedasPresentation {

    // MARK: - GO TO
    func goToNextView() {
    }

    // MARK: - PREPARE
    func retrieListOfCounties() {
        let _list =  self.interactor.retrieListOfCounties()
        self.view?.updateListOfCountries(countries: _list)
    }
}

// MARK: - TAGS
//extension ListaDeMonedasPresenter: ListaDeMonedasPresentationTags {
//    func trackStateShowScreen() {
//        TrackStateBuilder(
//            appScreenCategory: "ListaDeMonedas",
//            appScreenSubcategory: "ListaDeMonedas",
//            appSessionStatus: Constans.appSesionStatusLoggedIn,
//            trackState: "ListaDeMonedas")
//            .withUserDigitalId()
//            .send()
//    }
//
//    func trackActionMetadata(key: String, value: String) {
//        TrackActionBuilder(
//            appSessionStatus: Constans.appSesionStatusLoggedIn,
//            trackAction: Constans.trackActionMetadata)
//            .withUserDigitalId()
//            .withMetadataKey(key.unaccent())
//            .withMetadataValue(value.unaccent())
//            .send()
//    }
//
//    func trackActionClickButton(name: String, label: String) {
//        TrackActionBuilder(
//            appSessionStatus: Constans.appSesionStatusLoggedIn,
//            trackAction: Constans.trackAction)
//            .withUserDigitalId()
//            .withActionGroup("ListaDeMonedas")
//            .withActionCategory("ListaDeMonedas")
//            .withActionName(name.unaccent())
//            .withActionLabel(label.unaccent())
//            .send()
//    }
//
//    func trackEvent() {
//        TrackActionBuilder(
//            appSessionStatus: Constans.appSesionStatusLoggedIn,
//            trackAction: Constans.trackAction)
//            .withUserDigitalId()
//            .withEventName(Constans.appEventNameTrackAction)
//            .send()
//    }
//
//    func trackEventClickButton(name: String, label: String) {
//        TrackActionBuilder(
//            appSessionStatus: Constans.appSesionStatusLoggedIn,
//            trackAction: Constans.trackAction)
//            .withUserDigitalId()
//            .withEventName(Constans.appEventNameTrackAction)
//            .withActionGroup("ListaDeMonedas")
//            .withActionCategory("ListaDeMonedas")
//            .withActionName(name.unaccent())
//            .withActionLabel(label.unaccent())
//            .send()
//    }
//
//    func trackActionError(errorCode: String, errorMessage: String) {
//        TrackActionBuilder.showError(code: errorCode.unaccent(), message: errorMessage.unaccent())
//    }
//}
