//
//  Countries.swift
//  IOSChallengeBCP
//
//  Created by Jhonly Junior Garcia Pitoy on 16/12/21.
//

import Foundation


struct ListCountries: Codable {
    let valueofdolar: Double?
    let imagen: String?
    let moneda: String?
    let name: String?
}
struct Countries: Codable {
    let ListCountries: [ListCountries]?
}
